<?php

declare(strict_types=1);

namespace Psr\Clock;

use DateTimeImmutable;

class RealClock implements ClockInterface
{
    public function now(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}
