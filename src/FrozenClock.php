<?php

declare(strict_types=1);

namespace Psr\Clock;

use DateTimeImmutable;
use DateTimeZone;
use Exception;

/**
 * An implementation used for testing purposes, which takes a date as an argument and uses it when `now()` is called
 */
class FrozenClock implements ClockInterface
{
    private DateTimeImmutable $datetime;

    /**
     * @param string $datetimeAsString A datetime in Y-m-d H:i:s format
     */
    public function __construct(string $datetimeAsString)
    {
        $datetime = DateTimeImmutable::createFromFormat(
            'Y-m-d H:i:s',
            $datetimeAsString
        );
        if ($datetime === false) {
            throw new Exception(
                'Failed to build DateTimeImmutable from formatted string - please check $datetimeAsString format'
            );
        }
        $this->datetime = $datetime;
    }

    public function now(): DateTimeImmutable
    {
        return $this->datetime;
    }
}
