Clock Interface
==============

This repository holds clock interface related to [PSR-20 (Clock Interface)][psr-url].

And two implementations :
 - a real clock implementation
 - a "frozen" clock for testing purposes

[psr-url]: https://github.com/php-fig/fig-standards/blob/master/proposed/clock.md
